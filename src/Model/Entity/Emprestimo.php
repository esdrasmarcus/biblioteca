<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Emprestimo Entity
 *
 * @property int $id
 * @property int $livros_id
 * @property int $pessoas_id
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Livro $livro
 * @property \App\Model\Entity\Pessoa $pessoa
 */
class Emprestimo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'livros_id' => true,
        'pessoas_id' => true,
        'created' => true,
        'livro' => true,
        'pessoa' => true
    ];
}
